/*
 * crm114 backend for dovecot antispam plugin
 *
 * Copyright (C) 2004-2007  Johannes Berg <johannes@sipsolutions.net>
 *                    2006  Frank Cusack
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License Version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 */

#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <fcntl.h>

#include "lib.h"
#include "mail-storage-private.h"

#include "antispam-plugin.h"


static int call_reaver(const struct antispam_config *cfg,
		       const char *signature, enum classification wanted)
{
	pid_t pid;
	const char *class_arg;
	int pipes[2];

	switch (wanted) {
	case CLASS_NOTSPAM:
		class_arg = "--good";
		break;
	case CLASS_SPAM:
		class_arg = "--spam";
		break;
	}

	/*
	 * For reaver stdin, it wants to read a full message but
	 * really only needs the signature.
	 */
	if (pipe(pipes))
		return -1;

	pid = fork();
	if (pid < 0)
		return -1;

	if (pid) {
		int status;

		close(pipes[0]);

		/*
		 * Reaver wants the mail but only needs the cache ID
		 */
		write(pipes[1], cfg->crm.sigcfg.signature_hdr,
			strlen(cfg->crm.sigcfg.signature_hdr));
		write(pipes[1], ": ", 2);
		write(pipes[1], signature, strlen(signature));
		write(pipes[1], "\r\n\r\n", 4);
		close(pipes[1]);

		/*
		 * Wait for reaver
		 */
		waitpid(pid, &status, 0);
		if (!WIFEXITED(status))
			return 1;

		return WEXITSTATUS(status);
	} else {
		int fd = open("/dev/null", O_RDONLY);
		char **argv;
		/* 2 fixed, extra, terminating NULL */
		int sz = sizeof(char *) * (2 + cfg->crm.extra_args_num + 1);
		int i;

		argv = i_malloc(sz);
		memset(argv, 0, sz);

		close(0);
		close(1);
		close(2);
		/* see above */
		close(pipes[1]);

		if (dup2(pipes[0], 0) != 0)
			exit(1);
		close(pipes[0]);

		if (dup2(fd, 1) != 1)
			exit(1);
		if (dup2(fd, 2) != 2)
			exit(1);
		close(fd);

		argv[0] = (char *)cfg->crm.reaver_binary;
		argv[1] = (char *)class_arg;

		for (i = 0; i < cfg->crm.extra_args_num; i++)
			argv[i + 2] = (char *)cfg->crm.extra_args[i];

		debugv(&cfg->dbgcfg, argv);

		T_BEGIN {
		for (i = 0; i < cfg->crm.extra_env_num; i++) {
			char *name, *value;
			name = t_strdup_noconst(cfg->crm.extra_env[i]);
			value = strchr(name, '=');
			if (value) {
				*value = '\0';
				value++;
			}
			setenv(name, value, 1);
		}
		} T_END;

		execv(cfg->crm.reaver_binary, argv);
		/* fall through if reaver can't be found */
		debug(&cfg->dbgcfg, "executing %s failed: %d (uid=%d, gid=%d)",
			cfg->crm.reaver_binary, errno, getuid(), getgid());
		exit(127);
		/* not reached */
		return -1;
	}
}

struct antispam_transaction_context {
	struct siglist *siglist;
};

static struct antispam_transaction_context *
backend_start(const struct antispam_config *cfg ATTR_UNUSED,
	      struct mailbox *box ATTR_UNUSED)
{
	struct antispam_transaction_context *ast;

	ast = i_new(struct antispam_transaction_context, 1);
	ast->siglist = NULL;
	return ast;
}

static void backend_rollback(const struct antispam_config *cfg ATTR_UNUSED,
			     struct antispam_transaction_context *ast)
{
	signature_list_free(&ast->siglist);
	i_free(ast);
}

static int backend_commit(const struct antispam_config *cfg,
			  struct mailbox_transaction_context *ctx,
			  struct antispam_transaction_context *ast)
{
	struct siglist *item = ast->siglist;
	int ret = 0;

	while (item) {
		if (call_reaver(cfg, item->sig, item->wanted)) {
			ret = -1;
			mail_storage_set_error(ctx->box->storage,
					       ME(NOTPOSSIBLE)
					       "Failed to call reaver");
			break;
		}
		item = item->next;
	}

	signature_list_free(&ast->siglist);
	i_free(ast);
	return ret;
}

static int backend_handle_mail(const struct antispam_config *cfg,
			       struct mailbox_transaction_context *t,
			       struct antispam_transaction_context *ast,
			       struct mail *mail, enum classification want)
{
	return signature_extract_to_list(&cfg->crm.sigcfg, t, mail, &ast->siglist, want);
}

static void backend_init(struct antispam_config *cfg,
			 const char *(getenv)(const char *env, void *data),
			 void *getenv_data)
{
	const char *tmp;
	int i;

	tmp = getenv("CRM_BINARY", getenv_data);
	if (tmp) {
		cfg->crm.reaver_binary = tmp;
		debug(&cfg->dbgcfg, "reaver binary set to %s\n", tmp);
	} else
		cfg->crm.reaver_binary = "/bin/false";

	tmp = getenv("CRM_ARGS", getenv_data);
	if (tmp) {
		cfg->crm.extra_args = p_strsplit(cfg->mem_pool, tmp, ";");
		cfg->crm.extra_args_num = str_array_length(
					(const char *const *)cfg->crm.extra_args);
		for (i = 0; i < cfg->crm.extra_args_num; i++)
			debug(&cfg->dbgcfg, "reaver extra arg %s\n",
			      cfg->crm.extra_args[i]);
	}

	tmp = getenv("CRM_ENV", getenv_data);
	if (tmp) {
		cfg->crm.extra_env = p_strsplit(cfg->mem_pool, tmp, ";");
		cfg->crm.extra_env_num = str_array_length(
					(const char *const *)cfg->crm.extra_env);
		for (i = 0; i < cfg->crm.extra_env_num; i++)
			debug(&cfg->dbgcfg, "reaver env %s\n",
			      cfg->crm.extra_env[i]);
	}

	signature_init(&cfg->crm.sigcfg, &cfg->dbgcfg, getenv, getenv_data);
}

struct backend crm114_backend = {
	.init = backend_init,
	.handle_mail = backend_handle_mail,
	.start = backend_start,
	.rollback = backend_rollback,
	.commit = backend_commit,
};
