#!/bin/sh

VERSION=2.0

if head=$(git rev-parse --verify HEAD 2>/dev/null); then
	git update-index --refresh --unmerged > /dev/null
	descr=$(git describe)
	
	# on git builds check that the version number above
	# is correct...
	[ "${descr%%-*}" = "v$VERSION" ] || exit 2

	echo -n '#define ANTISPAM_VERSION "' > $1
	echo -n "${descr#v}" >> $1
	if git diff-index --name-only HEAD | read dummy ; then
		echo -n "-dirty" >> $1
	fi
	echo '"' >> $1
else
	echo '#define ANTISPAM_VERSION "'$VERSION'-notgit"' > $1
fi
